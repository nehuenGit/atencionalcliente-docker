package ar.com.experta.atencionalcliente.model;

/**
 * Created by Vatrox10 on 26/06/2017.
 */
public class ConsultaDiasBajaResponse extends AtencionAlClienteConsultaResponseAbs {
/*
    <element name="AS_VERIFICACION" type="string" db:index="2" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_HASH" type="string" db:index="4" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_POLIZA" type="string" db:index="5" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_DIAS_BAJA" type="string" db:index="8" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_DERIVAR" type="string" db:index="9" db:type="VARCHAR2" minOccurs="0" nillable="true"/>*/

    String dias_baja;

    public ConsultaDiasBajaResponse() {
    }

    public ConsultaDiasBajaResponse(String hash, String poliza, String derivar, String verificacion,String dias_baja) {
        this.dias_baja = dias_baja;
        this.hash = hash;
        this.poliza = poliza;
        this.derivar = derivar;
        this.verificacion = verificacion;
    }

    public String getDias_baja() {
        return dias_baja;
    }

    public void setDias_baja(String dias_baja) {
        this.dias_baja = dias_baja;
    }


}
