package ar.com.experta.atencionalcliente.model;

import ar.com.experta.atencionalcliente.utils.AtencionAlClienteUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.sql.Date;

/**
 * Created by Vatrox10 on 26/06/2017.
 */
@Component
abstract public class  AtencionAlClienteConsultaRequestAbs {

    String usuario;
    String verificacion;
    String pwid;
    String hash;
    String poliza;
    String cuil;
    String cuit;
    String nro_siniestro;
    String fecha_siniestro;
    AtencionAlClienteUtils alClienteUtils = AtencionAlClienteUtils.getInstance();


    public Date getFecha_siniestro() throws Exception{
        Date fecha= null;
        fecha= alClienteUtils.apiToDbDateConverter(fecha_siniestro);
        return fecha;
    }


    public void setFecha_siniestro(String fecha_siniestro) {
        this.fecha_siniestro = fecha_siniestro;
    }
    public String getUsuario() {
        return usuario;
    }
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    public String getVerificacion() {
        return verificacion;
    }
    public void setVerificacion(String verificacion) {
        this.verificacion = verificacion;
    }
    public int getPwid()  {
        if (StringUtils.isEmpty(pwid))
            return -1;
        return  Integer.parseInt(pwid);
    }
    public void setPwid(String pwid) {
        this.pwid = pwid;
    }
    public String getHash() {
        return hash;
    }
    public void setHash(String hash) {
        this.hash = hash;
    }
    public String getPoliza() {
        return poliza;
    }
    public void setPoliza(String poliza) {
        this.poliza = poliza;
    }
    public String getCuil() {
        return cuil;
    }
    public void setCuil(String cuil) {
        this.cuil = cuil;
    }

    public String getCuit() {
        return cuit;
    }

    public void setCuit(String cuit) {
        this.cuit = cuit;
    }

    public String getNro_siniestro() {
        return nro_siniestro;
    }

    public void setNro_siniestro(String nro_siniestro) {
        this.nro_siniestro = nro_siniestro;
    }
}
