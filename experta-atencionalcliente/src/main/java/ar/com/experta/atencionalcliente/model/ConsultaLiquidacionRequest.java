package ar.com.experta.atencionalcliente.model;

import ar.com.experta.atencionalcliente.utils.AtencionAlClienteUtils;

import java.sql.Date;

/**
 * Created by Vatrox10 on 26/06/2017.
 */
public class ConsultaLiquidacionRequest extends AtencionAlClienteConsultaRequestAbs{


    /*<sequence>
            <element name="AS_USUARIO" type="string" db:index="1" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_VERIFICACION" type="string" db:index="2" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_PWID" type="decimal" db:index="3" db:type="NUMBER" minOccurs="0" nillable="true"/>
            <element name="AS_HASH" type="string" db:index="4" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_POLIZA" type="string" db:index="5" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_CUIL" type="string" db:index="6" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_NRO_SINIESTRO" type="string" db:index="7" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AD_PERIODO_DESDE" type="dateTime" db:index="8" db:type="DATE" minOccurs="0" nillable="true"/>
            <element name="AD_PERIODO_HASTA" type="dateTime" db:index="9" db:type="DATE" minOccurs="0" nillable="true"/>
         </sequence>*/


    String periodo_desde;
    String periodo_hasta;

    Date periodo_desde_dateType;
    Date periodo_hasta_dateType;
    AtencionAlClienteUtils alClienteUtils = AtencionAlClienteUtils.getInstance();

    public ConsultaLiquidacionRequest() {
    }

    public ConsultaLiquidacionRequest(String usuario, String verificacion, String pwid, String hash, String poliza, String cuil, String nro_siniestro, String periodo_desde, String periodo_hasta) {
        this.periodo_desde = periodo_desde;
        this.periodo_hasta = periodo_hasta;
        this.usuario = usuario;
        this.verificacion = verificacion;
        this.pwid = pwid;
        this.hash = hash;
        this.poliza = poliza;
        this.cuil = cuil;
        this.nro_siniestro = nro_siniestro;
    }

    public Date getPeriodo_desde() throws Exception {
        return alClienteUtils.apiToDbDateConverter(periodo_desde);
    }

    public void setPeriodo_desde(String periodo_desde) throws Exception {
        this.periodo_desde = periodo_desde;
    }

    public Date getPeriodo_hasta() throws Exception {

        return alClienteUtils.apiToDbDateConverter(periodo_hasta);
    }

    public void setPeriodo_hasta(String periodo_hasta) throws Exception{

        this.periodo_hasta = periodo_hasta ;
    }
}
