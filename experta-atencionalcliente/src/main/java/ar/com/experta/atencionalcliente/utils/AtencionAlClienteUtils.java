package ar.com.experta.atencionalcliente.utils;

import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.Date;

/**
 * Created by Vatrox10 on 26/06/2017.
 */
public class AtencionAlClienteUtils {

    SimpleDateFormat formatDbToApi = new SimpleDateFormat("dd/MM/yy");

    private static AtencionAlClienteUtils atencionAlClienteUtilsInstance = new AtencionAlClienteUtils();

    private AtencionAlClienteUtils() {
    }
    public static AtencionAlClienteUtils getInstance(){
        return atencionAlClienteUtilsInstance;
    }

    public String dbToAppDateConverter(Date fechaDB)throws Exception{
        String fecha=null;

        if( ! StringUtils.isEmpty(fechaDB)) {
            fecha = formatDbToApi.format(fechaDB);
        }

        return fecha;
    }
    public Date apiToDbDateConverter(String fechaApp) throws Exception {
        Date fecha = null;

        if( ! StringUtils.isEmpty(fechaApp)) {

                fecha = new Date(formatDbToApi.parse(fechaApp).getTime());

        }

        return fecha;
    }
}
