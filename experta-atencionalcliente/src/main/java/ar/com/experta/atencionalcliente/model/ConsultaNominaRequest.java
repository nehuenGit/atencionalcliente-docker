package ar.com.experta.atencionalcliente.model;

/**
 * Created by Vatrox10 on 26/06/2017.
 */
public class ConsultaNominaRequest extends AtencionAlClienteConsultaRequestAbs{
/*
    <element name="AS_CUIL" type="string" db:index="1" db:default="true" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_USUARIO" type="string" db:index="2" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
*/

    public ConsultaNominaRequest() {
    }
    public ConsultaNominaRequest(String usuario, String cuil) {
        this.usuario = usuario;
        this.cuil = cuil;
    }
}
