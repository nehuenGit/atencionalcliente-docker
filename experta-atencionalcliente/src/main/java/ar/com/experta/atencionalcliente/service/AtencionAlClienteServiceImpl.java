package ar.com.experta.atencionalcliente.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import ar.com.experta.atencionalcliente.model.*;
import com.jcabi.aspects.Loggable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



@Service("atencionalcliente")
public class AtencionAlClienteServiceImpl  implements AtencionAlClienteService  {
	
	private final static Logger LOGGER = Logger.getLogger(AtencionAlClienteServiceImpl.class.getName());
	
	@PersistenceContext
	private EntityManager entityManager;



    @Override
    @Transactional
    @com.jcabi.aspects.Loggable
	public ConsultaAlicuotasResponse consultaAlicuotas(ConsultaAlicuotasRequest in) throws Exception{
		LOGGER.log(Level.INFO, "Entrando");

		StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("AP_ART_AC_CONS_ALICUOTAS");

		proc.registerStoredProcedureParameter("AS_USUARIO", String.class, ParameterMode.IN);
		proc.registerStoredProcedureParameter("AS_VERIFICACION", String.class, ParameterMode.INOUT);
		proc.registerStoredProcedureParameter("AS_PWID", Integer.class, ParameterMode.IN);
		proc.registerStoredProcedureParameter("AS_HASH", String.class, ParameterMode.INOUT);
		proc.registerStoredProcedureParameter("AS_POLIZA", String.class, ParameterMode.INOUT);
		proc.registerStoredProcedureParameter("AS_CUIT", String.class, ParameterMode.IN);
		proc.registerStoredProcedureParameter("AS_COMP_FIJO", String.class, ParameterMode.OUT);
		proc.registerStoredProcedureParameter("AS_COMP_VARIABLE", String.class, ParameterMode.OUT);
		proc.registerStoredProcedureParameter("AD_VIGENCIA", Date.class, ParameterMode.OUT);
		proc.registerStoredProcedureParameter("AS_DERIVAR", String.class, ParameterMode.OUT);
		
		
		proc.setParameter("AS_USUARIO", in.getUsuario());
		proc.setParameter("AS_VERIFICACION", in.getVerificacion());
		proc.setParameter("AS_PWID", in.getPwid());
		proc.setParameter("AS_HASH", in.getHash());
		proc.setParameter("AS_POLIZA", in.getPoliza());
		proc.setParameter("AS_CUIT", in.getCuit());
		proc.execute();


		
		LOGGER.log(Level.INFO, "Ejecutado");
		ConsultaAlicuotasResponse consultaAlicuotasResponse = new ConsultaAlicuotasResponse();

		consultaAlicuotasResponse.setVerificacion((String) proc.getOutputParameterValue("AS_VERIFICACION"));
		consultaAlicuotasResponse.setHash((String) proc.getOutputParameterValue("AS_HASH"));
		consultaAlicuotasResponse.setPoliza((String) proc.getOutputParameterValue("AS_POLIZA"));
		consultaAlicuotasResponse.setCompFijo((String) proc.getOutputParameterValue("AS_COMP_FIJO"));
		consultaAlicuotasResponse.setCompVariable((String) proc.getOutputParameterValue("AS_COMP_VARIABLE"));
		consultaAlicuotasResponse.setVigencia((Date) proc.getOutputParameterValue("AD_VIGENCIA"));
		consultaAlicuotasResponse.setDerivar((String) proc.getOutputParameterValue("AS_DERIVAR"));



		return consultaAlicuotasResponse;


	}

	@Override
    @Transactional
    @com.jcabi.aspects.Loggable
	public ConsultaDenunciaFormalResponse consultaDenunciaFormal(ConsultaDenunciaFormalRequest in) throws Exception{
		LOGGER.log(Level.INFO, "Entrando");
		StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("AP_ART_AC_CONS_DENUNCIA_FORMAL");

		proc.registerStoredProcedureParameter("AS_USUARIO", String.class, ParameterMode.IN);
		proc.registerStoredProcedureParameter("AS_VERIFICACION", String.class, ParameterMode.INOUT);
		proc.registerStoredProcedureParameter("AS_PWID", Integer.class, ParameterMode.IN);
		proc.registerStoredProcedureParameter("AS_HASH", String.class, ParameterMode.INOUT);
		proc.registerStoredProcedureParameter("AS_POLIZA", String.class, ParameterMode.INOUT);
		proc.registerStoredProcedureParameter("AS_CUIL", String.class, ParameterMode.IN);
		proc.registerStoredProcedureParameter("AS_NRO_SINIESTRO", String.class, ParameterMode.OUT);
		proc.registerStoredProcedureParameter("AD_FECHA_SINIESTRO", Date.class, ParameterMode.OUT);
		proc.registerStoredProcedureParameter("AD_FECHA_DENUN_FOR", Date.class, ParameterMode.OUT);
		proc.registerStoredProcedureParameter("AS_DERIVAR", String.class, ParameterMode.OUT);


		proc.setParameter("AS_USUARIO", in.getUsuario());
		proc.setParameter("AS_VERIFICACION", in.getVerificacion());
		proc.setParameter("AS_PWID", in.getPwid());
		proc.setParameter("AS_HASH", in.getHash());
		proc.setParameter("AS_POLIZA", in.getPoliza());
		proc.setParameter("AS_CUIL", in.getCuil());
		proc.execute();


		LOGGER.log(Level.FINE, "Ejecutado");
		ConsultaDenunciaFormalResponse consultaDenunciaFormalResponse = new ConsultaDenunciaFormalResponse();

		consultaDenunciaFormalResponse.setVerificacion((String) proc.getOutputParameterValue("AS_VERIFICACION"));
		consultaDenunciaFormalResponse.setHash((String) proc.getOutputParameterValue("AS_HASH"));
		consultaDenunciaFormalResponse.setPoliza((String) proc.getOutputParameterValue("AS_POLIZA"));
		consultaDenunciaFormalResponse.setNroSiniestro((String) proc.getOutputParameterValue("AS_NRO_SINIESTRO"));
		consultaDenunciaFormalResponse.setFechaSiniestro((Date) proc.getOutputParameterValue("AD_FECHA_SINIESTRO"));
		consultaDenunciaFormalResponse.setFechaDenunciaFormal((Date) proc.getOutputParameterValue("AD_FECHA_DENUN_FOR"));
		consultaDenunciaFormalResponse.setDerivar((String) proc.getOutputParameterValue("AS_DERIVAR"));


		return consultaDenunciaFormalResponse;
	}



    @Override
    @Transactional
    @com.jcabi.aspects.Loggable
    public ConsultaDiasBajaResponse consultaDiasBaja(ConsultaDiasBajaRequest in) throws Exception{

        LOGGER.log(Level.INFO, "Entrando");
        StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("AP_ART_AC_CONS_DIAS_BAJA");

        proc.registerStoredProcedureParameter("AS_USUARIO", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_VERIFICACION", String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_PWID", Integer.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_HASH", String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_POLIZA", String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_CUIL", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_NRO_SINIESTRO", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("AS_DIAS_BAJA", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("AS_DERIVAR", String.class, ParameterMode.OUT);


        proc.setParameter("AS_USUARIO", in.getUsuario());
        proc.setParameter("AS_VERIFICACION", in.getVerificacion());
        proc.setParameter("AS_PWID", in.getPwid());
        proc.setParameter("AS_HASH", in.getHash());
        proc.setParameter("AS_POLIZA", in.getPoliza());
        proc.setParameter("AS_CUIL", in.getCuil());
        proc.execute();


        LOGGER.log(Level.FINE, "Ejecutado");
        ConsultaDiasBajaResponse consultaDiasBajaResponse = new ConsultaDiasBajaResponse();

        consultaDiasBajaResponse.setVerificacion((String) proc.getOutputParameterValue("AS_VERIFICACION"));
        consultaDiasBajaResponse.setHash((String) proc.getOutputParameterValue("AS_HASH"));
        consultaDiasBajaResponse.setPoliza((String) proc.getOutputParameterValue("AS_POLIZA"));
        consultaDiasBajaResponse.setNroSiniestro((String) proc.getOutputParameterValue("AS_NRO_SINIESTRO"));
        consultaDiasBajaResponse.setDias_baja((String) proc.getOutputParameterValue("AS_DIAS_BAJA"));
        consultaDiasBajaResponse.setDerivar((String) proc.getOutputParameterValue("AS_DERIVAR"));


        return consultaDiasBajaResponse;
    }
    @Override
    @Transactional
    @com.jcabi.aspects.Loggable
    public ConsultaCobranzasResponse consultaCobranzas (ConsultaCobranzasRequest in) throws Exception{

        LOGGER.log(Level.INFO, "Entrando");
        StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("AP_ART_AC_CONS_EST_COBRANZAS");

        proc.registerStoredProcedureParameter("AS_USUARIO", 		    String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_VERIFICACION", 		String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_PWID", 		        Integer.class,ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_HASH",              String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_POLIZA",            String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_CUIT", 		        String.class,ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_ESTUDIO",           String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("AS_TELEFONO",          String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("AS_MAIL",              String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("AS_DERIVAR",           String.class, ParameterMode.OUT);


        proc.setParameter("AS_USUARIO", in.getUsuario());
        proc.setParameter("AS_VERIFICACION", in.getVerificacion());
        proc.setParameter("AS_PWID", in.getPwid());
        proc.setParameter("AS_HASH", in.getHash());
        proc.setParameter("AS_POLIZA", in.getPoliza());
        proc.setParameter("AS_CUIT", in.getCuit());
        proc.execute();


        LOGGER.log(Level.FINE, "Ejecutado");
        ConsultaCobranzasResponse consultaCobranzasResponse = new ConsultaCobranzasResponse();

        consultaCobranzasResponse.setVerificacion((String) proc.getOutputParameterValue("AS_VERIFICACION"));
        consultaCobranzasResponse.setHash((String) proc.getOutputParameterValue("AS_HASH"));
        consultaCobranzasResponse.setPoliza((String) proc.getOutputParameterValue("AS_POLIZA"));
        consultaCobranzasResponse.setEstudio((String) proc.getOutputParameterValue("AS_ESTUDIO"));
        consultaCobranzasResponse.setTelefono((String) proc.getOutputParameterValue("AS_TELEFONO"));
        consultaCobranzasResponse.setMail((String) proc.getOutputParameterValue("AS_MAIL"));
        consultaCobranzasResponse.setDerivar((String) proc.getOutputParameterValue("AS_DERIVAR"));


        return consultaCobranzasResponse;
    }
    @Override
    @Transactional
    @com.jcabi.aspects.Loggable
    public ConsultaNominaResponse consultaNomina(ConsultaNominaRequest in) throws Exception{

        LOGGER.log(Level.INFO, "Entrando");
        StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("AP_ART_AC_CONS_EST_NOM");

        proc.registerStoredProcedureParameter("AS_USUARIO", 	String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_CUIL", 		String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AD_FECHA_CARGA", Date.class,ParameterMode.OUT);
        proc.registerStoredProcedureParameter("AS_DERIVAR",     String.class, ParameterMode.OUT);


        proc.setParameter("AS_USUARIO", in.getUsuario());
        proc.setParameter("AS_CUIL", in.getCuil());
        proc.execute();


        LOGGER.log(Level.FINE, "Ejecutado");
        ConsultaNominaResponse consultaNominaResponse = new ConsultaNominaResponse();

        consultaNominaResponse.setFecha_carga((Date) proc.getOutputParameterValue("AD_FECHA_CARGA"));
        consultaNominaResponse.setDerivar((String) proc.getOutputParameterValue("AS_DERIVAR"));


        return consultaNominaResponse;
    }
    @Override
    @Transactional
    @com.jcabi.aspects.Loggable
    public ConsultaNominaLogResponse consultaNominaLog(ConsultaNominaLogRequest in) throws Exception{

        LOGGER.log(Level.INFO, "Entrando");
        StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("AP_ART_AC_CONS_EST_NOM_LOG");
                                                      /*as_verificacion in out varchar2,
                                                       as_usuario      in varchar2,
                                                       as_pwid         in art_login_bot.pwid%type,
                                                       as_hash         in out art_login_bot.hash%type,
                                                       as_poliza       in out t094_cuerpoliza_riesgo.npoliza%type ,
                                                       as_cuil         in siniestros.dni_empleado%type default null,
                                                       ad_fecha_carga  out date,
                                                       as_razon_social out personas.nombre%type,
                                                       as_derivar      out varchar2) is*/
        proc.registerStoredProcedureParameter("AS_USUARIO", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_VERIFICACION", String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_PWID", Integer.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_HASH", String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_POLIZA", String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_CUIL", String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_RAZON_SOCIAL", String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("AD_FECHA_CARGA", Date.class,ParameterMode.OUT);
        proc.registerStoredProcedureParameter("AS_DERIVAR",     String.class, ParameterMode.OUT);


        proc.setParameter("AS_USUARIO", in.getUsuario());
        proc.setParameter("AS_VERIFICACION", in.getVerificacion());
        proc.setParameter("AS_PWID", in.getPwid());
        proc.setParameter("AS_HASH", in.getHash());
        proc.setParameter("AS_POLIZA", in.getPoliza());
        proc.setParameter("AS_CUIL", in.getCuil());
        proc.execute();


        LOGGER.log(Level.FINE, "Ejecutado");
        ConsultaNominaLogResponse consultaNominaLogResponse = new ConsultaNominaLogResponse();

        consultaNominaLogResponse.setVerificacion((String) proc.getOutputParameterValue("AS_VERIFICACION"));
        consultaNominaLogResponse.setHash((String) proc.getOutputParameterValue("AS_HASH"));
        consultaNominaLogResponse.setPoliza((String) proc.getOutputParameterValue("AS_POLIZA"));
        consultaNominaLogResponse.setRazon_social((String) proc.getOutputParameterValue("AS_RAZON_SOCIAL"));
        consultaNominaLogResponse.setFecha_carga((Date) proc.getOutputParameterValue("AD_FECHA_CARGA"));
        consultaNominaLogResponse.setDerivar((String) proc.getOutputParameterValue("AS_DERIVAR"));


        return consultaNominaLogResponse;
    }
    @Override
    @Transactional
    @com.jcabi.aspects.Loggable
    public ConsultaLiquidacionResponse consultaLiquidacion(ConsultaLiquidacionRequest in) throws Exception{
/*create or replace PROCEDURE AP_ART_AC_CONS_ESTADO_LIQ(as_usuario       IN VARCHAR2,
                                                      as_verificacion  IN OUT VARCHAR2,
                                                      as_pwid          IN art_login_bot.pwid%TYPE,
                                                      as_hash          IN OUT art_login_bot.hash%TYPE,
                                                      as_poliza        IN OUT t094_cuerpoliza.npoliza%TYPE,
                                                      as_cuil          IN siniestros.dni_empleado%TYPE,
                                                      as_nro_siniestro IN OUT siniestros.expediente%TYPE,
                                                      ad_periodo_desde IN art_gestiones_ilt.reint_desde%TYPE,
                                                      ad_periodo_hasta IN art_gestiones_ilt.reint_hasta%TYPE,
                                                      as_estado_liq    OUT art_codigos.cod_descripcion%TYPE,
                                                      as_derivar       OUT VARCHAR2) IS*/
        LOGGER.log(Level.INFO, "Entrando");
        StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("AP_ART_AC_CONS_ESTADO_LIQ");

        proc.registerStoredProcedureParameter("AS_USUARIO", 		    String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_VERIFICACION", 		String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_PWID", 		        Integer.class,ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_HASH",              String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_POLIZA",            String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_CUIL",           String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_NRO_SINIESTRO",          String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AD_PERIODO_DESDE",              Date.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AD_PERIODO_HASTA",           Date.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_ESTADO_LIQ", String.class,ParameterMode.OUT);
        proc.registerStoredProcedureParameter("AS_DERIVAR",     String.class, ParameterMode.OUT);

        proc.setParameter("AS_USUARIO", in.getUsuario());
        proc.setParameter("AS_VERIFICACION", in.getVerificacion());
        proc.setParameter("AS_PWID", in.getPwid());
        proc.setParameter("AS_HASH", in.getHash());
        proc.setParameter("AS_POLIZA", in.getPoliza());
        proc.setParameter("AS_CUIL", in.getCuil());
        proc.setParameter("AS_NRO_SINIESTRO", in.getNro_siniestro());
        proc.setParameter("AD_PERIODO_DESDE", in.getPeriodo_desde());
        proc.setParameter("AD_PERIODO_HASTA", in.getPeriodo_hasta());
        proc.execute();


        LOGGER.log(Level.FINE, "Ejecutado");
        ConsultaLiquidacionResponse consultaLiquidacionResponse = new ConsultaLiquidacionResponse();

        consultaLiquidacionResponse.setVerificacion((String) proc.getOutputParameterValue("AS_VERIFICACION"));
        consultaLiquidacionResponse.setHash((String) proc.getOutputParameterValue("AS_HASH"));
        consultaLiquidacionResponse.setPoliza((String) proc.getOutputParameterValue("AS_POLIZA"));
        consultaLiquidacionResponse.setNroSiniestro((String) proc.getOutputParameterValue("AS_NRO_SINIESTRO"));
        consultaLiquidacionResponse.setEstado_liq((String) proc.getOutputParameterValue("AS_ESTADO_LIQ"));
        consultaLiquidacionResponse.setDerivar((String) proc.getOutputParameterValue("AS_DERIVAR"));


        return consultaLiquidacionResponse;
    }
    @Override
    @Transactional
    @com.jcabi.aspects.Loggable
    public ConsultaSiniestroResponse consultaSiniestro(ConsultaSiniestroRequest in) throws Exception{

        LOGGER.log(Level.INFO, "Entrando");
        StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("AP_ART_AC_CONS_ESTADO_STRO");

        proc.registerStoredProcedureParameter("AS_USUARIO", 		    String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_VERIFICACION", 		String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_PWID", 		        Integer.class,ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_HASH",              String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_POLIZA",            String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_CUIL",           String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_NRO_SINIESTRO",          String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AD_FECHA_SINIESTRO",          Date.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_ESTADO_STRO",              String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("AS_DERIVAR",           String.class, ParameterMode.OUT);


        proc.setParameter("AS_USUARIO", in.getUsuario());
        proc.setParameter("AS_VERIFICACION", in.getVerificacion());
        proc.setParameter("AS_PWID", in.getPwid());
        proc.setParameter("AS_HASH", in.getHash());
        proc.setParameter("AS_POLIZA", in.getPoliza());
        proc.setParameter("AS_CUIL", in.getCuil());
        proc.setParameter("AS_NRO_SINIESTRO", in.getNro_siniestro());
        proc.setParameter("AD_FECHA_SINIESTRO", in.getFecha_siniestro());
        proc.execute();


        LOGGER.log(Level.FINE, "Ejecutado");
        ConsultaSiniestroResponse consultaSiniestroResponse = new ConsultaSiniestroResponse();

        consultaSiniestroResponse.setVerificacion((String) proc.getOutputParameterValue("AS_VERIFICACION"));
        consultaSiniestroResponse.setHash((String) proc.getOutputParameterValue("AS_HASH"));
        consultaSiniestroResponse.setPoliza((String) proc.getOutputParameterValue("AS_POLIZA"));
        consultaSiniestroResponse.setNroSiniestro((String) proc.getOutputParameterValue("AS_NRO_SINIESTRO"));
        consultaSiniestroResponse.setEstado_stro((String) proc.getOutputParameterValue("AS_ESTADO_STRO"));
        consultaSiniestroResponse.setDerivar((String) proc.getOutputParameterValue("AS_DERIVAR"));


        return consultaSiniestroResponse;
    }
    @Override
    @Transactional
    @com.jcabi.aspects.Loggable
    public ConsultaFechaAudienciaResponse consultaFechaAudiencia(ConsultaFechaAudienciaRequest in) throws Exception{

        LOGGER.log(Level.INFO, "Entrando");
        StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("AP_ART_AC_CONS_FECHA_AUDIENCIA");

        proc.registerStoredProcedureParameter("AS_USUARIO", 		    String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_VERIFICACION", 		String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_PWID", 		        Integer.class,ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_HASH",              String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_POLIZA",            String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_CUIL",           String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_NRO_SINIESTRO",          String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AD_FECHA_AUDIENCIA",           Date.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("AS_DERIVAR",           String.class, ParameterMode.OUT);


        proc.setParameter("AS_USUARIO", in.getUsuario());
        proc.setParameter("AS_VERIFICACION", in.getVerificacion());
        proc.setParameter("AS_PWID", in.getPwid());
        proc.setParameter("AS_HASH", in.getHash());
        proc.setParameter("AS_POLIZA", in.getPoliza());
        proc.setParameter("AS_CUIL", in.getCuil());
        proc.setParameter("AS_NRO_SINIESTRO", in.getNro_siniestro());
        proc.execute();


        LOGGER.log(Level.FINE, "Ejecutado");
        ConsultaFechaAudienciaResponse consultaFechaAudienciaResponse= new ConsultaFechaAudienciaResponse();

        consultaFechaAudienciaResponse.setVerificacion((String) proc.getOutputParameterValue("AS_VERIFICACION"));
        consultaFechaAudienciaResponse.setHash((String) proc.getOutputParameterValue("AS_HASH"));
        consultaFechaAudienciaResponse.setPoliza((String) proc.getOutputParameterValue("AS_POLIZA"));
        consultaFechaAudienciaResponse.setFecha_audiencia((Date) proc.getOutputParameterValue("AD_FECHA_AUDIENCIA"));
        consultaFechaAudienciaResponse.setDerivar((String) proc.getOutputParameterValue("AS_DERIVAR"));


        return consultaFechaAudienciaResponse;
    }
    @Override
    @Transactional
    @com.jcabi.aspects.Loggable
    public ConsultaFechaDictamenResponse consultaFechaDictamen(ConsultaFechaDictamenRequest in) throws Exception{

        LOGGER.log(Level.INFO, "Entrando");
        StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("AP_ART_AC_CONS_FECHA_DICTAMEN");

        proc.registerStoredProcedureParameter("AS_USUARIO", 		    String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_VERIFICACION", 		String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_PWID", 		        Integer.class,ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_HASH",              String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_POLIZA",            String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_CUIL",           String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_NRO_SINIESTRO",          String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AD_FECHA_DICTAMEN",              Date.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("AS_DERIVAR",           String.class, ParameterMode.OUT);


        proc.setParameter("AS_USUARIO", in.getUsuario());
        proc.setParameter("AS_VERIFICACION", in.getVerificacion());
        proc.setParameter("AS_PWID", in.getPwid());
        proc.setParameter("AS_HASH", in.getHash());
        proc.setParameter("AS_POLIZA", in.getPoliza());
        proc.setParameter("AS_CUIL", in.getCuil());
        proc.setParameter("AS_NRO_SINIESTRO", in.getNro_siniestro());
        proc.execute();


        LOGGER.log(Level.FINE, "Ejecutado");
        ConsultaFechaDictamenResponse consultaFechaDictamenResponse = new ConsultaFechaDictamenResponse();

        consultaFechaDictamenResponse.setVerificacion((String) proc.getOutputParameterValue("AS_VERIFICACION"));
        consultaFechaDictamenResponse.setHash((String) proc.getOutputParameterValue("AS_HASH"));
        consultaFechaDictamenResponse.setPoliza((String) proc.getOutputParameterValue("AS_POLIZA"));
        consultaFechaDictamenResponse.setFecha_dictamen((Date) proc.getOutputParameterValue("AD_FECHA_DICTAMEN"));
        consultaFechaDictamenResponse.setDerivar((String) proc.getOutputParameterValue("AS_DERIVAR"));


        return consultaFechaDictamenResponse;
    }
    @Override
    @Transactional
    @com.jcabi.aspects.Loggable
    public ConsultaFechaReingresoResponse consultaFechaReingreso(ConsultaFechaReingresoRequest in) throws Exception{
/*create or replace PROCEDURE AP_ART_AC_CONS_FECHA_REINGRESO(as_usuario          IN VARCHAR2,
                                                        as_verificacion     IN OUT VARCHAR2,
                                                        as_pwid             IN art_login_bot.pwid%TYPE,
                                                        as_hash             IN OUT art_login_bot.hash%TYPE,
                                                        as_poliza           IN OUT t094_cuerpoliza.npoliza%TYPE,
                                                        as_cuil             IN siniestros.cuit%TYPE,
                                                        as_nro_siniestro    IN OUT siniestros.expediente%TYPE,
                                                        ad_fecha_siniestro  IN siniestros.fecha_siniestro%TYPE,
                                                        ad_fecha_reingreso  OUT art_reingresos.fecha_reingreso%TYPE,
                                                        as_nombre_acc       OUT personas.nombre%TYPE,
                                                        as_derivar          OUT VARCHAR2) IS*/
        LOGGER.log(Level.INFO, "Entrando");
        StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("AP_ART_AC_CONS_FECHA_REINGRESO");

        proc.registerStoredProcedureParameter("AS_USUARIO", 		    String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_VERIFICACION", 		String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_PWID", 		        Integer.class,ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_HASH",              String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_POLIZA",            String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_CUIL",           String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_NRO_SINIESTRO",          String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AD_FECHA_SINIESTRO",           Date.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AD_FECHA_REINGRESO",          Date.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("AS_NOMBRE_ACC",              String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("AS_DERIVAR",           String.class, ParameterMode.OUT);


        proc.setParameter("AS_USUARIO", in.getUsuario());
        proc.setParameter("AS_VERIFICACION", in.getVerificacion());
        proc.setParameter("AS_PWID", in.getPwid());
        proc.setParameter("AS_HASH", in.getHash());
        proc.setParameter("AS_POLIZA", in.getPoliza());
        proc.setParameter("AS_CUIL", in.getCuil());
        proc.setParameter("AS_NRO_SINIESTRO", in.getNro_siniestro());
        proc.setParameter("AD_FECHA_SINIESTRO", in.getFecha_siniestro());
        proc.execute();


        LOGGER.log(Level.FINE, "Ejecutado");
        ConsultaFechaReingresoResponse consultaFechaReingresoResponse = new ConsultaFechaReingresoResponse();

        consultaFechaReingresoResponse.setVerificacion((String) proc.getOutputParameterValue("AS_VERIFICACION"));
        consultaFechaReingresoResponse.setHash((String) proc.getOutputParameterValue("AS_HASH"));
        consultaFechaReingresoResponse.setPoliza((String) proc.getOutputParameterValue("AS_POLIZA"));
        consultaFechaReingresoResponse.setNroSiniestro((String) proc.getOutputParameterValue("AS_NRO_SINIESTRO"));
        consultaFechaReingresoResponse.setFecha_reingreso((Date) proc.getOutputParameterValue("AD_FECHA_REINGRESO"));
        consultaFechaReingresoResponse.setNombre_acc((String) proc.getOutputParameterValue("AS_NOMBRE_ACC"));
        consultaFechaReingresoResponse.setDerivar((String) proc.getOutputParameterValue("AS_DERIVAR"));


        return consultaFechaReingresoResponse;
    }
    @Override
    @Transactional
    @com.jcabi.aspects.Loggable
    public ConsultaMotivoRechazoResponse consultaMotivoRechazo(ConsultaMotivoRechazoRequest in) throws Exception{

        LOGGER.log(Level.INFO, "Entrando");
        StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("AP_ART_AC_CONS_MOTIVO_RECHAZO");

        proc.registerStoredProcedureParameter("AS_USUARIO", 		    String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_VERIFICACION", 		String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_PWID", 		        Integer.class,ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_HASH",              String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_POLIZA",            String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_CUIL",           String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_NRO_SINIESTRO",          String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AD_FECHA_SINIESTRO",           Date.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_NOMBRE",          String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("AS_MOTIVO_RECHAZO",              String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("AS_DERIVAR",           String.class, ParameterMode.OUT);


        proc.setParameter("AS_USUARIO", in.getUsuario());
        proc.setParameter("AS_VERIFICACION", in.getVerificacion());
        proc.setParameter("AS_PWID", in.getPwid());
        proc.setParameter("AS_HASH", in.getHash());
        proc.setParameter("AS_POLIZA", in.getPoliza());
        proc.setParameter("AS_CUIL", in.getCuil());
        proc.setParameter("AS_NRO_SINIESTRO", in.getNro_siniestro());
        proc.setParameter("AD_FECHA_SINIESTRO", in.getFecha_siniestro());
        proc.execute();


        LOGGER.log(Level.FINE, "Ejecutado");
        ConsultaMotivoRechazoResponse consultaMotivoRechazoResponse = new ConsultaMotivoRechazoResponse();

        consultaMotivoRechazoResponse.setVerificacion((String) proc.getOutputParameterValue("AS_VERIFICACION"));
        consultaMotivoRechazoResponse.setHash((String) proc.getOutputParameterValue("AS_HASH"));
        consultaMotivoRechazoResponse.setPoliza((String) proc.getOutputParameterValue("AS_POLIZA"));
        consultaMotivoRechazoResponse.setNroSiniestro((String) proc.getOutputParameterValue("AS_NRO_SINIESTRO"));
        consultaMotivoRechazoResponse.setNombre((String) proc.getOutputParameterValue("AS_NOMBRE"));
        consultaMotivoRechazoResponse.setMotivo_rechazo((String) proc.getOutputParameterValue("AS_MOTIVO_RECHAZO"));
        consultaMotivoRechazoResponse.setDerivar((String) proc.getOutputParameterValue("AS_DERIVAR"));


        return consultaMotivoRechazoResponse;
    }
    @Override
    @Transactional
    @com.jcabi.aspects.Loggable
    public ConsultaNroContratoResponse consultaNroContrato(ConsultaNroContratoRequest in) throws Exception{

        LOGGER.log(Level.INFO, "Entrando");
        StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("AP_ART_AC_CONS_NRO_CONTRATO");

        proc.registerStoredProcedureParameter("AS_USUARIO", 		    String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_VERIFICACION", 		String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_PWID", 		        Integer.class,ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_HASH",              String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_CUIT",           String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_NRO_CONTRATO",           String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("AS_RAZON_SOCIAL",          String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("AS_VIGENCIA",              String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("AS_MOTIVO",              String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("AS_DERIVAR",           String.class, ParameterMode.OUT);

        proc.setParameter("AS_USUARIO", in.getUsuario());
        proc.setParameter("AS_VERIFICACION", in.getVerificacion());
        proc.setParameter("AS_PWID", in.getPwid());
        proc.setParameter("AS_HASH", in.getHash());
        proc.setParameter("AS_CUIT", in.getCuit());
        proc.execute();


        LOGGER.log(Level.FINE, "Ejecutado");
        ConsultaNroContratoResponse consultaNroContratoResponse = new ConsultaNroContratoResponse();

        consultaNroContratoResponse.setNro_contrato((String) proc.getOutputParameterValue("AS_NRO_CONTRATO"));
        consultaNroContratoResponse.setRazon_social((String) proc.getOutputParameterValue("AS_RAZON_SOCIAL"));
        consultaNroContratoResponse.setVigencia((String) proc.getOutputParameterValue("AS_VIGENCIA"));
        consultaNroContratoResponse.setMotivo((String) proc.getOutputParameterValue("AS_MOTIVO"));
        consultaNroContratoResponse.setDerivar((String) proc.getOutputParameterValue("AS_DERIVAR"));


        return consultaNroContratoResponse;
    }
    @Override
    @Transactional
    @com.jcabi.aspects.Loggable
    public ConsultaNroSiniestroResponse consultaNroSiniestro(ConsultaNroSiniestroRequest in) throws Exception{

        LOGGER.log(Level.INFO, "Entrando");
        StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("AP_ART_AC_CONS_NRO_SINIESTRO");

        proc.registerStoredProcedureParameter("AD_FECHA_SINIESTRO",      Date.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_CUIL",          		 String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_NRO_SINIESTRO",        String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("AS_DERIVAR",              String.class, ParameterMode.OUT);


        proc.setParameter("AD_FECHA_SINIESTRO", in.getFecha_siniestro());
        proc.setParameter("AS_CUIL", in.getCuil());
        proc.execute();


        LOGGER.log(Level.FINE, "Ejecutado");
        ConsultaNroSiniestroResponse consultaNroSiniestroResponse = new ConsultaNroSiniestroResponse();

        consultaNroSiniestroResponse.setNroSiniestro((String) proc.getOutputParameterValue("AS_NRO_SINIESTRO"));
        consultaNroSiniestroResponse.setDerivar((String) proc.getOutputParameterValue("AS_DERIVAR"));


        return consultaNroSiniestroResponse;
    }
    @Override
    @Transactional
    @com.jcabi.aspects.Loggable
    public ConsultaPrestadorMedicoResponse consultaPrestadorMedico(ConsultaPrestadorMedicoRequest in) throws Exception{

        LOGGER.log(Level.INFO, "Entrando");
        StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("AP_ART_AC_CONS_PREST_MEDICO");

        proc.registerStoredProcedureParameter("AS_USUARIO", 		    String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_VERIFICACION", 		String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_PWID", 		        Integer.class,ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_HASH",              String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_POLIZA",            String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_CUIL",           String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_NRO_SINIESTRO",        String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AD_FECHA_SINIESTRO",           Date.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_PRESTADOR_MEDICO",          String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("AS_DERIVAR",           String.class, ParameterMode.OUT);


        proc.setParameter("AS_USUARIO", in.getUsuario());
        proc.setParameter("AS_VERIFICACION", in.getVerificacion());
        proc.setParameter("AS_PWID", in.getPwid());
        proc.setParameter("AS_HASH", in.getHash());
        proc.setParameter("AS_POLIZA", in.getPoliza());
        proc.setParameter("AS_CUIL", in.getCuil());
        proc.setParameter("AS_NRO_SINIESTRO", in.getNro_siniestro());
        proc.setParameter("AD_FECHA_SINIESTRO", in.getFecha_siniestro());
        proc.execute();


        LOGGER.log(Level.FINE, "Ejecutado");
        ConsultaPrestadorMedicoResponse consultaPrestadorMedicoResponse = new ConsultaPrestadorMedicoResponse();

        consultaPrestadorMedicoResponse.setVerificacion((String) proc.getOutputParameterValue("AS_VERIFICACION"));
        consultaPrestadorMedicoResponse.setHash((String) proc.getOutputParameterValue("AS_HASH"));
        consultaPrestadorMedicoResponse.setPoliza((String) proc.getOutputParameterValue("AS_POLIZA"));
        consultaPrestadorMedicoResponse.setNroSiniestro((String) proc.getOutputParameterValue("AS_NRO_SINIESTRO"));
        consultaPrestadorMedicoResponse.setPrestador_medico((String) proc.getOutputParameterValue("AS_PRESTADOR_MEDICO"));
        consultaPrestadorMedicoResponse.setDerivar((String) proc.getOutputParameterValue("AS_DERIVAR"));


        return consultaPrestadorMedicoResponse;
    }
    @Override
    @Transactional
    @com.jcabi.aspects.Loggable
    public ConsultaProximoTurnoResponse consultaProximoTurno(ConsultaProximoTurnoRequest in) throws Exception{

        LOGGER.log(Level.INFO, "Entrando");
        StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("AP_ART_AC_CONS_PROX_TURNO");

        proc.registerStoredProcedureParameter("AS_USUARIO", 		    String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_VERIFICACION", 		String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_PWID", 		        Integer.class,ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_HASH",              String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_POLIZA",            String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_NRO_SINIESTRO",        String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_CUIL",           String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_PROX_TURNO_PREST",          String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("AS_DERIVAR",           String.class, ParameterMode.OUT);


        proc.setParameter("AS_USUARIO", in.getUsuario());
        proc.setParameter("AS_VERIFICACION", in.getVerificacion());
        proc.setParameter("AS_PWID", in.getPwid());
        proc.setParameter("AS_HASH", in.getHash());
        proc.setParameter("AS_POLIZA", in.getPoliza());
        proc.setParameter("AS_CUIL", in.getCuil());
        proc.setParameter("AS_NRO_SINIESTRO", in.getNro_siniestro());
        proc.execute();


        LOGGER.log(Level.FINE, "Ejecutado");
        ConsultaProximoTurnoResponse consultaProximoTurnoResponse = new ConsultaProximoTurnoResponse();

        consultaProximoTurnoResponse.setVerificacion((String) proc.getOutputParameterValue("AS_VERIFICACION"));
        consultaProximoTurnoResponse.setHash((String) proc.getOutputParameterValue("AS_HASH"));
        consultaProximoTurnoResponse.setPoliza((String) proc.getOutputParameterValue("AS_POLIZA"));
        consultaProximoTurnoResponse.setProx_turno_prest((String) proc.getOutputParameterValue("AS_PROX_TURNO_PREST"));
        consultaProximoTurnoResponse.setDerivar((String) proc.getOutputParameterValue("AS_DERIVAR"));


        return consultaProximoTurnoResponse;
    }
    @Override
    @Transactional
    @com.jcabi.aspects.Loggable
    public ConsultaSaldoResponse consultaSaldo(ConsultaSaldoRequest in) throws Exception{

        LOGGER.log(Level.INFO, "Entrando");
        StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("AP_ART_AC_CONS_SALDO");

        proc.registerStoredProcedureParameter("AS_USUARIO", 		    String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_VERIFICACION", 		String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_PWID", 		        Integer.class,ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_HASH",              String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_POLIZA",            String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_CUIT",           String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_CAPITAL",           String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("AS_INTERES",          String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("AS_DERIVAR",           String.class, ParameterMode.OUT);


        proc.setParameter("AS_USUARIO", in.getUsuario());
        proc.setParameter("AS_VERIFICACION", in.getVerificacion());
        proc.setParameter("AS_PWID", in.getPwid());
        proc.setParameter("AS_HASH", in.getHash());
        proc.setParameter("AS_POLIZA", in.getPoliza());
        proc.setParameter("AS_CUIT", in.getCuit());
        proc.execute();


        LOGGER.log(Level.FINE, "Ejecutado");
        ConsultaSaldoResponse consultaSaldoResponse = new ConsultaSaldoResponse();

        consultaSaldoResponse.setVerificacion((String) proc.getOutputParameterValue("AS_VERIFICACION"));
        consultaSaldoResponse.setHash((String) proc.getOutputParameterValue("AS_HASH"));
        consultaSaldoResponse.setPoliza((String) proc.getOutputParameterValue("AS_POLIZA"));
        consultaSaldoResponse.setCapital((String) proc.getOutputParameterValue("AS_CAPITAL"));
        consultaSaldoResponse.setInteres((String) proc.getOutputParameterValue("AS_INTERES"));
        consultaSaldoResponse.setDerivar((String) proc.getOutputParameterValue("AS_DERIVAR"));


        return consultaSaldoResponse;
    }


    @Override
    @Transactional
    @com.jcabi.aspects.Loggable
    public ConsultaUsuarioExpertaResponse consultaUsuarioExperta(ConsultaUsuarioExpertaRequest in) throws Exception{

        LOGGER.log(Level.INFO, "Entrando");
        StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("AP_ART_AC_CONS_USUARIO_EXPERTA");

        proc.registerStoredProcedureParameter("AS_USUARIO", 		    String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_DNI", 		String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_EXISTE_USUARIO",          String.class, ParameterMode.OUT);
        proc.registerStoredProcedureParameter("AS_DERIVAR",           String.class, ParameterMode.OUT);


        proc.setParameter("AS_USUARIO", in.getUsuario());
        proc.setParameter("AS_DNI", in.getDni());
        proc.execute();


        LOGGER.log(Level.FINE, "Ejecutado");
        ConsultaUsuarioExpertaResponse consultaUsuarioExpertaResponse = new ConsultaUsuarioExpertaResponse();

        consultaUsuarioExpertaResponse.setExiste_usuario((String) proc.getOutputParameterValue("AS_EXISTE_USUARIO"));
        consultaUsuarioExpertaResponse.setDerivar((String) proc.getOutputParameterValue("AS_DERIVAR"));


        return consultaUsuarioExpertaResponse;
    }
    @Override
    @Transactional
    @com.jcabi.aspects.Loggable
    public ConsultaVerificacionAccResponse consultaVerificacionAcc(ConsultaVerificacionAccRequest in) throws Exception{
/*create or replace PROCEDURE AP_ART_AC_VERIF_ACC(as_usuario         IN VARCHAR2,
                                                as_verificacion    IN OUT VARCHAR2,
                                                as_nro_siniestro   IN siniestros.expediente%TYPE,
                                                as_cuil            IN siniestros.dni_empleado%TYPE,
                                                as_tipo_accidente  IN siniestros.tipo_expediente%TYPE,
                                                ad_fecha_siniestro IN siniestros.fecha_siniestro%TYPE,
                                                as_cp              IN personas.cdpostal%TYPE) IS*/
        LOGGER.log(Level.INFO, "Entrando");
        StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("AP_ART_AC_VERIF_ACC");

        proc.registerStoredProcedureParameter("AS_USUARIO", 		    String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_VERIFICACION", 		String.class, ParameterMode.INOUT);
        proc.registerStoredProcedureParameter("AS_NRO_SINIESTRO",        String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_CUIL",           String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_TIPO_ACCIDENTE",           String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AD_FECHA_SINIESTRO",           Date.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter("AS_CP",           String.class, ParameterMode.IN);

        proc.setParameter("AS_USUARIO", in.getUsuario());
        proc.setParameter("AS_VERIFICACION", in.getVerificacion());
        proc.setParameter("AS_NRO_SINIESTRO", in.getNro_siniestro());
        proc.setParameter("AS_CUIL", in.getCuil());
        proc.setParameter("AS_TIPO_ACCIDENTE", in.getTipo_accidente());
        proc.setParameter("AD_FECHA_SINIESTRO", in.getFecha_siniestro());
        proc.setParameter("AS_CP", in.getCp());
        proc.execute();


        LOGGER.log(Level.FINE, "Ejecutado");
        ConsultaVerificacionAccResponse consultaVerificacionAccResponse = new ConsultaVerificacionAccResponse();


        consultaVerificacionAccResponse.setVerificacion((String) proc.getOutputParameterValue("AS_VERIFICACION"));

        return consultaVerificacionAccResponse;
    }


}