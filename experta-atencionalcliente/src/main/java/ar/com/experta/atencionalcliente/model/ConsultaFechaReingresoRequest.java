package ar.com.experta.atencionalcliente.model;


/**
 * Created by Vatrox10 on 26/06/2017.
 */
public class ConsultaFechaReingresoRequest extends AtencionAlClienteConsultaRequestAbs {
    /*<complexType>
         <sequence>
            <element name="AS_USUARIO" type="string" db:index="1" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_VERIFICACION" type="string" db:index="2" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_PWID" type="decimal" db:index="3" db:type="NUMBER" minOccurs="0" nillable="true"/>
            <element name="AS_HASH" type="string" db:index="4" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_POLIZA" type="string" db:index="5" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_CUIL" type="string" db:index="6" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_NRO_SINIESTRO" type="string" db:index="7" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AD_FECHA_SINIESTRO" type="dateTime" db:index="8" db:type="DATE" minOccurs="0" nillable="true"/>
         </sequence>
      </complexType>*/



    public ConsultaFechaReingresoRequest() {
    }

    public ConsultaFechaReingresoRequest(String usuario, String verificacion, String pwid, String hash, String poliza, String cuil, String nro_siniestro, String fecha_siniestro) {
        this.fecha_siniestro = fecha_siniestro;
        this.usuario = usuario;
        this.verificacion = verificacion;
        this.pwid = pwid;
        this.hash = hash;
        this.poliza = poliza;
        this.cuil = cuil;
        this.nro_siniestro = nro_siniestro;
    }

}
