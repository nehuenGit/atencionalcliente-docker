package ar.com.experta.atencionalcliente.model;

/**
 * Created by Vatrox10 on 27/06/2017.
 */
public class ConsultaVerificacionAccRequest extends AtencionAlClienteConsultaRequestAbs{
    /* <sequence>
            <element name="AS_USUARIO" type="string" db:index="1" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_VERIFICACION" type="string" db:index="2" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_NRO_SINIESTRO" type="string" db:index="3" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_CUIL" type="string" db:index="4" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_TIPO_ACCIDENTE" type="string" db:index="5" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AD_FECHA_SINIESTRO" type="dateTime" db:index="6" db:type="DATE" minOccurs="0" nillable="true"/>
            <element name="AS_CP" type="string" db:index="7" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
         </sequence>*/

    String cp;
    String tipo_accidente;

    public ConsultaVerificacionAccRequest() {
    }

    public ConsultaVerificacionAccRequest(String usuario, String verificacion,String fecha_siniestro, String cuil, String nro_siniestro, String cp, String tipo_accidente) {
        this.cp = cp;
        this.tipo_accidente = tipo_accidente;
        this.usuario = usuario;
        this.verificacion = verificacion;
        this.cuil = cuil;
        this.nro_siniestro = nro_siniestro;
        this.fecha_siniestro = fecha_siniestro;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getTipo_accidente() {
        return tipo_accidente;
    }

    public void setTipo_accidente(String tipo_accidente) {
        this.tipo_accidente = tipo_accidente;
    }
}
