package ar.com.experta.atencionalcliente.model;

/**
 * Created by Vatrox10 on 26/06/2017.
 */
public class ConsultaNroContratoResponse extends AtencionAlClienteConsultaResponseAbs {
    /*<complexType>
         <sequence>
            <element name="AS_NRO_CONTRATO" type="string" db:index="6" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_RAZON_SOCIAL" type="string" db:index="7" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_VIGENCIA" type="string" db:index="8" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_MOTIVO" type="string" db:index="9" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_DERIVAR" type="string" db:index="10" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
         </sequence>
      </complexType>*/

    String nro_contrato;
    String vigencia;
    String razon_social;
    String motivo;

    public ConsultaNroContratoResponse() {
    }

    public ConsultaNroContratoResponse(String derivar, String nro_contrato, String vigencia, String razon_social, String motivo) {
        this.nro_contrato = nro_contrato;
        this.vigencia = vigencia;
        this.razon_social = razon_social;
        this.motivo = motivo;
        this.derivar = derivar;
    }

    public String getNro_contrato() {
        return nro_contrato;
    }

    public void setNro_contrato(String nro_contrato) {
        this.nro_contrato = nro_contrato;
    }

    public String getVigencia() {
        return vigencia;
    }

    public void setVigencia(String vigencia) {
        this.vigencia = vigencia;
    }

    public String getRazon_social() {
        return razon_social;
    }

    public void setRazon_social(String razon_social) {
        this.razon_social = razon_social;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }
}
