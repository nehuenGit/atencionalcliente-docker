package ar.com.experta.atencionalcliente.model;

/**
 * Created by Vatrox10 on 26/06/2017.
 */
public class ConsultaCobranzasResponse extends AtencionAlClienteConsultaResponseAbs{
  /*  <element name="AS_VERIFICACION" type="string" db:index="2" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_HASH" type="string" db:index="4" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_POLIZA" type="string" db:index="6" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_ESTUDIO" type="string" db:index="7" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_TELEFONO" type="string" db:index="8" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_MAIL" type="string" db:index="9" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_DERIVAR" type="string" db:index="10" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
*/


    String estudio;
    String telefono;
    String mail;

    public ConsultaCobranzasResponse(String hash, String poliza, String derivar, String verificacion, String estudio, String telefono, String mail) {
        this.estudio = estudio;
        this.telefono = telefono;
        this.mail = mail;
        this.hash = hash;
        this.poliza = poliza;
        this.derivar = derivar;
        this.verificacion = verificacion;
    }

    public ConsultaCobranzasResponse() {
    }

    public String getEstudio() {
        return estudio;
    }

    public void setEstudio(String estudio) {
        this.estudio = estudio;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
