package ar.com.experta.atencionalcliente;

import ar.com.experta.atencionalcliente.monitoring.ControllerInterceptor;
import ar.com.experta.atencionalcliente.monitoring.ServiceInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.CommonsRequestLoggingFilter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;



@SpringBootApplication
public class ExpertaAtencionalclienteApplication  {



	public static void main(String[] args) {

		SpringApplication.run(ExpertaAtencionalclienteApplication.class, args);


	}
}
