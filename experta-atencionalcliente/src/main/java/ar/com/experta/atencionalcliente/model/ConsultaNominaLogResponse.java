package ar.com.experta.atencionalcliente.model;

import ar.com.experta.atencionalcliente.utils.AtencionAlClienteUtils;

import java.sql.Date;

/**
 * Created by Vatrox10 on 26/06/2017.
 */
public class ConsultaNominaLogResponse extends AtencionAlClienteConsultaResponseAbs {

    /*<sequence>
            <element name="AS_VERIFICACION" type="string" db:index="1" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_HASH" type="string" db:index="4" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_POLIZA" type="string" db:index="5" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AD_FECHA_CARGA" type="dateTime" db:index="7" db:type="DATE" minOccurs="0" nillable="true"/>
            <element name="AS_RAZON_SOCIAL" type="string" db:index="8" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_DERIVAR" type="string" db:index="9" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
         </sequence>*/


    String fecha_carga;
    String razon_social;
    AtencionAlClienteUtils alClienteUtils = AtencionAlClienteUtils.getInstance();

    public ConsultaNominaLogResponse() {
    }

    public ConsultaNominaLogResponse(String hash, String poliza, String derivar, String verificacion,String fecha_carga, String razon_social) {
        this.fecha_carga = fecha_carga;
        this.razon_social = razon_social;
        this.hash = hash;
        this.poliza = poliza;
        this.derivar = derivar;
        this.verificacion = verificacion;
    }

    public String getFecha_carga() {
        return fecha_carga;
    }

    public void setFecha_carga(Date fecha_carga) throws Exception{
        this.fecha_carga = alClienteUtils.dbToAppDateConverter(fecha_carga);
    }

    public String getRazon_social() {
        return razon_social;
    }

    public void setRazon_social(String razon_social) {
        this.razon_social = razon_social;
    }
}
