package ar.com.experta.atencionalcliente.model;

import ar.com.experta.atencionalcliente.utils.AtencionAlClienteUtils;

import java.sql.Date;

public class ConsultaDenunciaFormalResponse extends AtencionAlClienteConsultaResponseAbs {


	String fechaSiniestro;
	String fechaDenunciaFormal;
	AtencionAlClienteUtils atencionAlClienteUtils = AtencionAlClienteUtils.getInstance();

	public ConsultaDenunciaFormalResponse(String hash, String poliza, String derivar, String verificacion,String fechaSiniestro, String fechaDenunciaFormal) {
		this.fechaSiniestro = fechaSiniestro;
		this.fechaDenunciaFormal = fechaDenunciaFormal;
		this.hash = hash;
		this.poliza = poliza;
		this.derivar = derivar;
		this.verificacion = verificacion;
	}


	public ConsultaDenunciaFormalResponse() {
	}


	public String getFechaSiniestro() {
		return fechaSiniestro;
	}
	public void setFechaSiniestro(Date fechaSiniestro) throws Exception{
			this.fechaSiniestro = atencionAlClienteUtils.dbToAppDateConverter(fechaSiniestro);
	}
	public String getFechaDenunciaFormal() {
		return fechaDenunciaFormal;
	}
	public void setFechaDenunciaFormal(Date fechaDenunciaFormal) throws Exception{
		this.fechaDenunciaFormal = atencionAlClienteUtils.dbToAppDateConverter(fechaDenunciaFormal);

	}
}
