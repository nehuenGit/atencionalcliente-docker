package ar.com.experta.atencionalcliente.model;

/**
 * Created by Vatrox10 on 26/06/2017.
 */
public class ConsultaMotivoRechazoResponse extends AtencionAlClienteConsultaResponseAbs {
    /*<sequence>
            <element name="AS_VERIFICACION" type="string" db:index="2" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_HASH" type="string" db:index="4" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_POLIZA" type="string" db:index="5" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_NRO_SINIESTRO" type="string" db:index="7" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_NOMBRE" type="string" db:index="9" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_MOTIVO_RECHAZO" type="string" db:index="10" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_DERIVAR" type="string" db:index="11" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
         </sequence>
      </complexType>*/

    String nombre;
    String motivo_rechazo;

    public ConsultaMotivoRechazoResponse() {
    }

    public ConsultaMotivoRechazoResponse(String hash, String poliza, String derivar, String verificacion, String nroSiniestro,String nombre, String motivo_rechazo) {
        this.nombre = nombre;
        this.motivo_rechazo = motivo_rechazo;
        this.hash = hash;
        this.poliza = poliza;
        this.derivar = derivar;
        this.verificacion = verificacion;
        this.nroSiniestro = nroSiniestro;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMotivo_rechazo() {
        return motivo_rechazo;
    }

    public void setMotivo_rechazo(String motivo_rechazo) {
        this.motivo_rechazo = motivo_rechazo;
    }
}
