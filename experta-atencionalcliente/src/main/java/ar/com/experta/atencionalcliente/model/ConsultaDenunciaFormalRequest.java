package ar.com.experta.atencionalcliente.model;

public class ConsultaDenunciaFormalRequest extends AtencionAlClienteConsultaRequestAbs {
	

	public ConsultaDenunciaFormalRequest(String usuario, String verificacion, String pwid, String hash, String poliza, String cuil) {
		this.usuario = usuario;
		this.verificacion = verificacion;
		this.pwid = pwid;
		this.hash = hash;
		this.poliza = poliza;
		this.cuil = cuil;
	}

	public ConsultaDenunciaFormalRequest() {
	}



}
