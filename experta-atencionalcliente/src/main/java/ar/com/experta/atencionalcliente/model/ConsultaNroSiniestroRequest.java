package ar.com.experta.atencionalcliente.model;

/**
 * Created by Vatrox10 on 26/06/2017.
 */
public class ConsultaNroSiniestroRequest extends AtencionAlClienteConsultaRequestAbs{
 /* <complexType>
         <sequence>
            <element name="AS_CUIL" type="string" db:index="1" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AD_FECHA_SINIESTRO" type="dateTime" db:index="2" db:type="DATE" minOccurs="0" nillable="true"/>
         </sequence>
      </complexType>*/
 public ConsultaNroSiniestroRequest() {
 }
    public ConsultaNroSiniestroRequest(String cuil, String fecha_siniestro) {
        this.cuil = cuil;
        this.fecha_siniestro = fecha_siniestro;
    }

}
