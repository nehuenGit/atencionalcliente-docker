package ar.com.experta.atencionalcliente.model;

/**
 * Created by Vatrox10 on 26/06/2017.
 */
public class ConsultaNroSiniestroResponse extends AtencionAlClienteConsultaResponseAbs {
    /*<sequence>
            <element name="AS_NRO_SINIESTRO" type="string" db:index="3" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_DERIVAR" type="string" db:index="4" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
         </sequence>*/

    public ConsultaNroSiniestroResponse() {

    }
    public ConsultaNroSiniestroResponse( String derivar, String nroSiniestro) {
        this.derivar = derivar;
        this.nroSiniestro = nroSiniestro;
    }

}
