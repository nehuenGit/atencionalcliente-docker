package ar.com.experta.atencionalcliente.model;

/**
 * Created by Vatrox10 on 26/06/2017.
 */
public class ConsultaProximoTurnoResponse extends AtencionAlClienteConsultaResponseAbs {
    /*<sequence>
            <element name="AS_VERIFICACION" type="string" db:index="2" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_HASH" type="string" db:index="4" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_POLIZA" type="string" db:index="5" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_PROX_TURNO_PREST" type="string" db:index="8" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_DERIVAR" type="string" db:index="9" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
         </sequence>*/

    String prox_turno_prest;

    public ConsultaProximoTurnoResponse() {
    }

    public ConsultaProximoTurnoResponse(String hash, String poliza, String derivar, String verificacion,String prox_turno_prest) {
        this.prox_turno_prest = prox_turno_prest;
        this.hash = hash;
        this.poliza = poliza;
        this.derivar = derivar;
        this.verificacion = verificacion;
    }
    public String getProx_turno_prest() {
        return prox_turno_prest;
    }

    public void setProx_turno_prest(String prox_turno_prest) {
        this.prox_turno_prest = prox_turno_prest;
    }
}
