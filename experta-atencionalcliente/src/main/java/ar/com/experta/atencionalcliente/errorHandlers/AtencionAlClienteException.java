package ar.com.experta.atencionalcliente.errorHandlers;

public class AtencionAlClienteException extends Exception {
	private static final long serialVersionUID = 1L;

	private String errorCode;
	private String errorMessage;
	private String location;
	private String runtime;
	
	
	public AtencionAlClienteException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}
	public AtencionAlClienteException() {
		super();
	}
	
	public AtencionAlClienteException(String errorCode, String errorMessage, String location, String runtime){
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.location = location;
		this.runtime = runtime;
	}

	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	public String getRuntime() {
		return runtime;
	}
	public void setRuntime(String runtime) {
		this.runtime = runtime;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
}
