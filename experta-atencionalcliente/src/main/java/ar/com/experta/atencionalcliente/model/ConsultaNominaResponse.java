package ar.com.experta.atencionalcliente.model;

import ar.com.experta.atencionalcliente.utils.AtencionAlClienteUtils;

import java.sql.Date;

/**
 * Created by Vatrox10 on 26/06/2017.
 */
public class ConsultaNominaResponse extends AtencionAlClienteConsultaResponseAbs{
/*
    <element name="AD_FECHA_CARGA" type="dateTime" db:index="3" db:type="DATE" minOccurs="0" nillable="true"/>
            <element name="AS_DERIVAR" type="string" db:index="4" db:type="VARCHAR2" minOccurs="0" nillable="true"/>*/

        String fecha_carga;

        AtencionAlClienteUtils atencionAlClienteUtils = AtencionAlClienteUtils.getInstance();

    public ConsultaNominaResponse() {
    }

    public ConsultaNominaResponse(String derivar, String fecha_carga) {
        this.fecha_carga = fecha_carga;
        this.derivar = derivar;
    }
    public String getFecha_carga() {
        return fecha_carga;
    }

    public void setFecha_carga(Date fecha_carga) throws Exception{
        this.fecha_carga = atencionAlClienteUtils.dbToAppDateConverter(fecha_carga);
    }


}
