package ar.com.experta.atencionalcliente.model;

/**
 * Created by Vatrox10 on 27/06/2017.
 */
public class ConsultaSaldoRequest extends AtencionAlClienteConsultaRequestAbs{
    /*<sequence>
            <element name="AS_USUARIO" type="string" db:index="1" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_VERIFICACION" type="string" db:index="2" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_PWID" type="decimal" db:index="3" db:type="NUMBER" minOccurs="0" nillable="true"/>
            <element name="AS_HASH" type="string" db:index="4" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_POLIZA" type="string" db:index="5" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_CUIT" type="string" db:index="6" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
         </sequence>*/

    public ConsultaSaldoRequest() {
    }
    public ConsultaSaldoRequest(String usuario, String verificacion, String pwid, String hash, String poliza, String cuit) {
        this.usuario = usuario;
        this.verificacion = verificacion;
        this.pwid = pwid;
        this.hash = hash;
        this.poliza = poliza;
        this.cuit = cuit;
    }
}
