# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
experta_atencionalcliente_springboot
* Version
1.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
Steps to build and run this application

* mvn clean install --settings settings.xml
* cd ./experta-atencionalcliente/target
* docker build -t microservicedemo/atencioncliente .
### Donde -t especifica el tag con el que se bautiza la imagen

* docker run -d -p 8080:8090 -t --name ac microservicedemo/atencioncliente
### Donde -d ejecuta como demon, -p define el puerto, 8080:8090 indica el puerto externo de la imagen y el puerto interno con el que se conecta.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact